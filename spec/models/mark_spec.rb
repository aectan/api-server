require 'rails_helper'

RSpec.describe Mark, type: :model do
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:mark_id) }
  it { should validate_presence_of(:name_rus) }
  it { should validate_presence_of(:translit) }

  describe 'Uniqueness validate' do
    subject { create(:mark) }
    it { should validate_uniqueness_of(:name) }
    it { should validate_uniqueness_of(:mark_id) }
    it { should validate_uniqueness_of(:name_rus) }
    it { should validate_uniqueness_of(:translit) }
  end
end
