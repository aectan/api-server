FactoryBot.define do
  factory :mark do
    sequence(:name) { |n| "Name_#{n}" }
    sequence(:mark_id) { |n| n }
    sequence(:name_rus) { |n| "Name_rus_#{n}" }
    sequence(:translit) { |n| "Translit_#{n}" }
  end
end
