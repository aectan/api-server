require 'rails_helper'

describe Auto::Marks do
  context 'GET /api/v1/auto/marks' do
    before { create_list(:mark, 3) }
    it 'returns statuses' do
      get '/api/v1/auto/marks'
      expect(response.status).to eq(200)
    end

    it 'returns array of marks' do
      get '/api/v1/auto/marks'
      marks = JSON.parse(response.body, symbolize_names: true)[:data]

      marks.each do |mark|
        expect(mark[:type]).to eq 'marks'
        expect(mark[:attributes].keys)
          .to match_array(%i[name mark_id name_rus translit])
      end

      expect(Mark.pluck(:id).map(&:to_s))
        .to match_array(marks.map { |mark| mark[:id] })
    end
  end
end
