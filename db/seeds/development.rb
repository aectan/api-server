def seed_marks
  marks_dump = Rails.root.join('db', 'dumps', 'marks.json')
  file = File.read(marks_dump)
  data_hash = JSON.parse(file, symbolize_names: true)
  write_or_update_marks(data_hash[:records])
end

def write_or_update_marks(records)
  progressbar = ProgressBar.create(title: 'Marks', total: records.count)

  records.each do |record|
    Mark.where(mark_id: record[:mark_id]).first_or_initialize.tap do |mark|
      mark.name = record[:name]
      mark.name_rus = record[:name_rus]
      mark.translit = record[:translit]
      mark.save
    end

    progressbar.increment
  end
end

seed_marks
