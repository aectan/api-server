# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_12_06_193248) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "marks", force: :cascade do |t|
    t.integer "mark_id", null: false
    t.string "name", null: false
    t.string "name_rus", null: false
    t.string "translit", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["mark_id"], name: "index_marks_on_mark_id", unique: true
    t.index ["name"], name: "index_marks_on_name", unique: true
    t.index ["name_rus"], name: "index_marks_on_name_rus", unique: true
    t.index ["translit"], name: "index_marks_on_translit", unique: true
  end

end
