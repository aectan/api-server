class CreateMarks < ActiveRecord::Migration[5.2]
  def change
    create_table :marks do |t|
      t.integer :mark_id, null: false, index: { unique: true }
      t.string :name, null: false, index: { unique: true }
      t.string :name_rus, null: false, index: { unique: true }
      t.string :translit, null: false, index: { unique: true }
      t.timestamps
    end
  end
end
