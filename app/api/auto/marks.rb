# frozen_string_literal: true

module Auto
  # Auto marks
  class Marks < BaseApi
    resource :auto do
      get :marks do
        Mark.all
      end
    end
  end
end
