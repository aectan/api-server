class Mark < ApplicationRecord
  validates :name,
            :mark_id,
            :name_rus,
            :translit,
            presence: true,
            uniqueness: true
end
